/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      dynamicMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dynamicFvMesh dynamicMotionSolverFvMesh;

motionSolverLibs ("libfvMotionSolvers.so" "libRBFMeshMotionSolver.so");

solver      displacementLaplacian;
//solver      velocityLaplacian;

//"solver|motionSolver" RBFMeshMotionSolver; // radial basis function solver


// Settings for the RBF solver
RBFMeshMotionSolverCoeffs {
    staticPatches    (top bottom);
    movingPatches    (cylinder);
    fixedPatches     (inlet outlet);
    interpolation
    {
        function     WendlandC2;
        radius       1e-2;
        polynomial   true;
        cpu          true;
        fullCPU      true;
    }
    coarsening
    {
        enabled                 false;
        tol                     1e-2;
        minPoints               100;
        maxPoints               10000;
        livePointSelection      false;
        exportSelectedPoints    false;
    }
}

// ************************************************************************* //


displacementLaplacianCoeffs
{
    diffusivity  inverseDistance  (square);
}


sixDoFRigidBodyMotionCoeffs
{
    patches         (cylinder);
    innerDistance   0.4;
    outerDistance   4.0;

    mass            117.10;
    centreOfMass    (0.0 0.0 0.0);
    momentOfInertia (0.049087 0.049087 1.0);
    orientation
    (
    1 0 0
    0 1 0
    0 0 1
    );

    velocity        (0.0  0.0  0.0);
    angularMomentum (0.0  0.0  0.0);
    g               (0.0  0.0  0.0);

    rho             rhoInf;
    rhoInf          1;
    report          on;
    reportToFile    on;

    accelerationRelaxation 1.0;
    accelerationDamping    1.0;
    
    solver
    {
        type    Newmark;
        gamma   0.5;    // Velocity integration coefficient
        beta    0.25;   // Position integration coefficient
    }

    constraints
    {
        xyplane
        {
            sixDoFRigidBodyMotionConstraint   plane;
            normal                            (0 0 1);
        }

        myorientation
        {
            sixDoFRigidBodyMotionConstraint   orientation;
            centreOfRotation                  (0 0 0);
        }

        yLine
        {
            sixDoFRigidBodyMotionConstraint line;
            centreOfRotation    (0.0 0.0 0.0);
            direction           (0 1 0);
        }
    }

    restraints
    {
        verticalSpring
        {
            sixDoFRigidBodyMotionRestraint linearSpring;

            anchor          (0.0 0.0 0.0);
            refAttachmentPt (0.0 0.0 0.0);
            stiffness       184.92;
            damping         0.35317;
            restLength      0;
        }
    }
}



// ************************************************************************* //
