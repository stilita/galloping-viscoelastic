#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 16:49:36 2023

@author: claudio
"""

import numpy as np


points = np.genfromtxt('points.txt', skip_header=1, delimiter=',')
cells = np.genfromtxt('cells.txt', skip_header=1, delimiter=',')

with open('mesh.dat', 'w') as fp:
    fp.write("# PROGRAM NUVOLA GRID\n")
    
    
    num_points = points.shape[0]
    
    fp.write(" {0}\n".format(num_points))
    
    for ii in range(num_points):
        fp.write("{0:.8f}\t{1:.8f}\t{2:.8f}\n".format(*points[ii,1:4]))
        
    num_cells = cells.shape[0]
    
    fp.write(" {0}\n".format(num_cells))
    
    for jj in range(num_cells):
        
        curr_points = cells[jj,1:]+1
        
        np = len(curr_points)
        
        for ip in range(np):
            if ip < np-1:
                fp.write("{0:d}\t".format(int(curr_points[ip])))
            else:
                fp.write("{0:d}\n".format(int(curr_points[ip])))
    
