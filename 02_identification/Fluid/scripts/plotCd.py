import numpy as np
import matplotlib.pyplot as plt

a = np.genfromtxt('../postProcessing/forceCoeffs1/0/coefficient_0.dat')


samples = 2000
Re = 'Re120_01'


cd_mean = np.mean(a[-samples:,1])
cd_std  = np.std(a[-samples:,1])

plt.figure(figsize=(24,12))
plt.subplot(2,1,1)
plt.plot(a[:,0], a[:,1])
plt.xlabel('t')
plt.ylabel(r'$C_d$')

plt.ylim([cd_mean -3*cd_std, cd_mean + 3*cd_std])

plt.subplot(2,1,2)
plt.plot(a[:,0], a[:,4])
plt.xlabel('t')
plt.ylabel(r'$C_l$')

plt.savefig('cdcl_{0}.png'.format(Re))



print('Average Cd of last {0:d} samples: {1} - std dev {2}'.format(samples, cd_mean, cd_std))

print('Average Cl of last {0:d} samples: {1} - std dev {2}'.format(samples,np.mean(a[-samples:,4]), np.std(a[-samples:,4])))
