# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 09:55:22 2024

@author: claudio.caccia
"""

import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('../postProcessing/forces/0/force.dat', comments='#')

fig, (ax1, ax2) = plt.subplots(2, figsize=(16, 10))

fig.suptitle('Forces on square')
color = 'tab:red'
ax1.set_xlabel('time (s)')
ax1.plot(data[:, 0], data[:, 1], label='$F_x$', color=color)
ax1.set_ylim([-1., 3])
ax1.set_ylabel('$F_x$', color=color)
ax1.tick_params(axis='y', labelcolor=color)

color = 'tab:blue'
ax1b = ax1.twinx()
ax1b.plot(data[:, 0], 0.4*np.sin(2*np.pi*1*data[:, 0]), '-.', label='displacement', color=color)
ax1b.set_ylabel('$A \sin(\omega t)$', color=color)
ax1b.tick_params(axis='y', labelcolor=color)

color = 'tab:red'
ax1.set_xlabel('time (s)')
ax2.plot(data[:, 0], data[:, 2], color=color)
ax2.set_ylim([-25, 25])
ax2.set_ylabel('$F_y$', color=color)
ax2.tick_params(axis='y', labelcolor=color)


color = 'tab:blue'
ax2b = ax2.twinx()
ax2b.plot(data[:, 0], 0.4*np.sin(2*np.pi*1*data[:, 0]), '-.', color=color)
ax2b.set_ylabel('$A \sin(\omega t)$', color=color)
ax2b.tick_params(axis='y', labelcolor=color)
ax2b.set_ylim([-0.8, 0.8])

plt.savefig('forces.png')